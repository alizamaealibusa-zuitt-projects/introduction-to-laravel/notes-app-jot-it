@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <h1>CREATE NOTE</h1>
        </div>
        <div class="col-md-8">
          <form method="POST" action="/posts">
            @csrf
            <div class="form-group">
              <label for="title">Title:</label>
              <input type="text" class="form-control" id="title" name="title">
            </div>
            <div class="form-group">
              <label for="content">Content:</label>
              <input type="text" class="form-control" id="content" name="content">
            </div>
            <button type="submit" class="btn btn-primary">Create Note</button>
          </form>
        </div>
    </div>
</div>
@endsection
