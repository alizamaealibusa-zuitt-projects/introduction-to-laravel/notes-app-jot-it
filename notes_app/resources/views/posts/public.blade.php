@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <h1>PUBLIC NOTES</h1>
        </div>
        <div class="col-md-8">
          @foreach ($posts as $post)
            @if (!$post->is_active)
            <div class="row">
              <div class="card mb-3" style="width:100%;">
                <div class="card-body">
                  <h3 class="card-title">
                    <a id="{{ $post->id }}" href="/posts/{{ $post->id }}">{{ $post->title }}</a>
                    <small class="badge badge-pill badge-secondary">{{ $post->is_active ? '' : 'public' }}</small>
                  </h3>
                  <p class="card-text">{{ $post->content }}</p>
                </div>
              </div>
            </div>
            @endif
          @endforeach
        </div>
    </div>
</div>
@endsection
