<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;
use Auth;

class PostController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth')->except(['index', 'show']);
    }
    
    // Create a new note
        // Endpoint: GET /posts/create
        public function create()
        {
            return view('posts.create');
        }

        // Endpoint: POST /posts
        public function store(Request $req)
        {
            $new_post = new Post([
                'title' => $req->input('title'),
                'content' => $req->input('content'),
                'user_id' => Auth::user()->id
            ]);
            $new_post->save();
            return redirect('/posts');
        }

    // Retrieve specific note
        // Endpoint: GET /posts/{post_id}
        public function show($post_id)
        {
            // Retrieve a specific post
            $post = Post::find($post_id);
            return view('posts.show')->with('post', $post);
        }

    // Get all user's notes
        // Endpoint: GET /posts
        public function index()
        {
            $my_posts = Auth::user()->posts;
            return view('posts.index')->with('posts', $my_posts);
        }

    // Edit
        // Endpoint: GET /posts/{post_id}/edit
        public function edit($post_id)
        {
            // Find the post to be updated
            $existing_post = Post::find($post_id);
            // Redirect the user to page where the post will be updated
            return view('posts.edit')->with('post', $existing_post);
        }

        // Endpoint: PUT /posts/{post_id}
        public function update($post_id, Request $req)
        {
            // Find an existing post to be updated
            $existing_post = Post::find($post_id);
            // Set the new values of an existing post
            $existing_post->title = $req->input('title');
            $existing_post->content = $req->input('content');
            $existing_post->save();
            // Redirect the user to the page of individual post
            return redirect("/posts/$post_id");
        }

    // Delete
        // Endpoint: DELETE /posts/{post_id}
        public function destroy($post_id)
        {
            // Find the existing post to be deleted
            $existing_post = Post::find($post_id);
            // Delete the post
            $existing_post->delete();
            // Redirect the user somewhere
            return redirect('/posts');
        }

    // Make Public
        // Endpoint: /posts/{post_id}/public
        public function makePublic($post_id)
        {
            $existing_post = Post::find($post_id);
            $existing_post->is_active = false;
            $existing_post->save();
            return redirect("/posts/$post_id");
        }

        // Endpoint: /posts/public
        public function publicPosts()
        {
            $public_posts = Post::all();
            return view('posts.public')->with('posts', $public_posts);
        }

    // Make Private
        // Endpoint: /posts/{post_id}/private
        public function makePrivate($post_id)
        {
            $existing_post = Post::find($post_id);
            $existing_post->is_active = true;
            $existing_post->save();
            return redirect("/posts/$post_id");
        }


}
