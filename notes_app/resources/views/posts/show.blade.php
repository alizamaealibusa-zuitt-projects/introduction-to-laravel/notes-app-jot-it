@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="row">
              <div class="card mb-3" style="width:100%;">
                <div class="card-body">
                  <div class="card-title">
                    <h1>{{ $post->title }} <small class="badge badge-pill badge-secondary">{{ $post->is_active ? '' : 'public' }}</small></h1>
                    <h6>by: {{ $post->user->name }}</h6>
                  </div>
                  <h5 class="card-text">{{ $post->content }}</h5>
                  <div class="container">
                    <div class="row">
                      <div class="col-md-6">
                        <div class="row justify-content-start">
                        <a class="btn btn-light btn-sm" href="/posts#{{ $post->id }}">Back</a>
                      </div>
                      </div>
                        <div class="col-md-6">
                        @if (Auth::check() && (Auth::user()->id === $post->user_id))
                            <div class="row justify-content-end">
                              <a href="/posts/{{ $post->id }}/edit" class="btn btn-primary btn-sm mr-1">Edit</a>
                              <form method="POST" action="/posts/{{ $post->id }}">
                                @method('DELETE')
                                @csrf
                                <button type="submit" class="btn btn-danger btn-sm mr-1">Delete</button>
                              </form>
                            @if ($post->is_active)
                              <form method="POST" action="/posts/{{ $post->id }}/public">
                                @method('PUT')
                                @csrf
                                <button type="submit" class="btn btn-warning btn-sm">Make Public</button>
                              </form>
                            @else
                              <form method="POST" action="/posts/{{ $post->id }}/private">
                                @method('PUT')
                                @csrf
                                <button type="submit" class="btn btn-warning btn-sm">Make Private</button>
                              </form>
                            @endif
                            </div>
                        @endif
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
        </div>
    </div>
</div>
@endsection